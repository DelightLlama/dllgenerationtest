#pragma once

#ifndef EXT_MYLIB

	#ifdef DDLL_BUILD
		#define EXT_MYLIB __declspec(dllexport)
	#else
		#pragma comment(lib, "MyLib.lib")
		#define EXT_MYLIB __declspec(dllimport)
	#endif // DDLL_BUILD


#endif // !EXT_MYLIB

class MyLib
{
};


