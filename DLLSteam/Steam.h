#pragma once

#include <Godot.hpp>
#include <Node.hpp>

namespace godot {
	class Steam : public Node {
		private :
			GODOT_CLASS(Steam, Node)
		public :
			static void _register_methods();
			void _init();
			String get_platfrom();
	};
}
