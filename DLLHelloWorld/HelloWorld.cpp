#include "HelloWorld.h"

using namespace godot;

void HelloWorld::_register_methods() {
	register_method((char*)"hello_world", &HelloWorld::hello_world);
}

void HelloWorld::_init() {

}

String HelloWorld::hello_world() {
	return "Hello World!";
}