#pragma once

#include <Godot.hpp>
#include <Node.hpp>

namespace godot {
	class Epic : public Node {
	private:
		GODOT_CLASS(Epic, Node)
	public:
		static void _register_methods();
		void _init();
		String get_platfrom();
	};
}

